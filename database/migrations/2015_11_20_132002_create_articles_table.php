<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('slug')->unique();
            $table->integer('user_id')->unsigned();
            $table->text('content');
            $table->integer('image_id')->nullable()->unsigned();
            $table->integer('status_id')->default(0)->unsigned();
            $table->integer('type_id')->default(1)->unsigned();
            $table->integer('series_id')->default(1)->unsigned();
            $table->integer('parent_id')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('articles');
    }
}
