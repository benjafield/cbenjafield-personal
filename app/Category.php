<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    
	/**
	 * The table the model uses
	 */
	protected $table = 'categories';

	/**
	 * The fields that are mass assignable
	 */
	protected $fillable = [ 'name', 'description' ];

}