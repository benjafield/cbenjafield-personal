<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    
    /**
     * The table the model uses
     */
    protected $table = 'articles';

    /**
     * Fields that may be mass assigned
     */
	protected $fillable = [ 'title', 'slug', 'content', 'image_id', 'series_id', 'parent_id', 'published_at' ];

	/**
	 * An article can belong to ONE user.
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * An article has one featured image
	 */
	public function image()
	{
		return $this->hasOne('App\File');
	}

	/**
	 * An article can have one series
	 */
	public function series()
	{
		return $this->hasOne('App\Series');
	}

	/**
	 * An article can have one parent
	 */
	public function parent()
	{
		return $this->hasOne('App\Article');
	}

}
