<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{
    
	/**
	 * Acts as the root for the controller
	 */
	public function index()
	{

		return view('blog.articles.default');

	}

	/**
	 * Show an indivdual article by the slug in URL
	 *
	 * @param string $slug
	 */
	public function show($slug)
	{

		return view('blog.articles.show');

	}

}
