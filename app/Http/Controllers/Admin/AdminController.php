<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    
    /**
     * The root function for the admin section.
     */
	public function index()
	{

		return view('admin.dashboard');

	}

}
