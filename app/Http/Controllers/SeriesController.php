<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class SeriesController extends Controller
{
    
	/**
	 * Act as root for the controller
	 */
	public function index()
	{

		return view('blog.series.default');

	}

	/**
	 * Show an individual series
	 */
	public function show($id)
	{

		return view('blog.series.show');

	}

}
