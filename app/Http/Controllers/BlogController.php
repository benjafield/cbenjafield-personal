<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    
	/**
	 * Act as root for the controller
	 */
	public function index()
	{

		$articles = Article::latest('published_at')->with('user')->get();

		return view('blog.default', compact('articles'));

	}

}
