<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    
	/**
	 * Acts as the root for the controller
	 */
	public function index()
	{

		return view('single-page-home');

	}

	/**
	 * Handle the contact form submission
	 */
	public function contactform(Request $request)
	{

		$this->validate($request, [
			'name' => 'required|min:2',
			'email' => 'required|email',
			'message' => 'required|min:5'
		]);

		Mail::raw(print_r($request->input(), TRUE), function ($m) use ($request) {
			$m->from($request->input('email'), $request->input('name'));
			$m->to('me@cbenjafield.com')->subject('C Benjafield Website Message');
		});

		if ($request->ajax())
		{
			return response()->json([ 'status' => 200 ]);
		}

		return back()->with(['msg_status' => true, 'msg' => 'Thanks, your message has been sent']);

	}

}
