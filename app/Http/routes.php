<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Site Area
 */
get('/', 'SiteController@index');
post('/', 'SiteController@contactform');

/**
 * Rambles Area
 */
get('rambles', 'BlogController@index');

/**
 * Series
 */
get('rambles/series', 'SeriesController@index');
get('rambles/series/{id}', 'SeriesController@show')->where('id', '[0-9]+');

/**
 * Articles
 */
get('rambles/articles', 'ArticlesController@index');
get('rambles/articles/{slug}', 'ArticlesController@show')->where('slug', '[a-z0-9_-]+');

/**
 * Authentication
 */
get('26/03/2011/login', 'Auth\AuthController@getLogin');
get('26/03/2011/logout', 'Auth\AuthController@getLogout');
post('26/03/2011/login', 'Auth\AuthController@postLogin');

/**
 * Admin
 */
get('26/03/2011/admin', 'Admin\AdminController@index');