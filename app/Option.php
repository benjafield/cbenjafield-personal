<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    
	/**
	 * The table the model uses
	 */
	protected $table = 'options';

	/**
	 * The mass assignments
	 */
	protected $fillable = [ 'key', 'value' ];

}
