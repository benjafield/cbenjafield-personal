<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    
	/**
	 * The series table
	 */
	protected $table = 'series';

	/**
	 * Fields that can be mass assignable
	 */
	protected $fillable = [ 'title', 'slug', 'image_id', 'description' ];

	/**
	 * A series belongs to a user
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	/**
	 * A series can have one image
	 */
	public function image()
	{
		return $this->hasOne('App\File');
	}

}
