<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    
	/**
	 * The table the model will use
	 */
	protected $table = 'files';

	/**
	 * The mass assignable fields
	 */
	protected $fillable = [ 'name', 'location', 'mime_type', 'alternative', 'title' ];

}
