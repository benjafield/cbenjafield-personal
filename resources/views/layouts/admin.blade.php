<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>@yield('title') / Charlie Benjafield</title>
	{!! Html::style('css/admin.css') !!}
</head>
<body>
	@if(Auth::check())
	<header class="Header">
		<h1 class="Logo"><a href="{{ url('26/03/2011/admin') }}">Rambles</a></h1>
	</header>
	@endif

	@yield('content')
</body>
</html>