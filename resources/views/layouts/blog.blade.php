<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<title>@yield('title') &#9924; Charlie's Rambles</title>
	{!! Html::style('assets/css/lnr.css') !!}
	{!! Html::style('css/app.css') !!}
</head>
<body class="@yield('body_class')" id="csstyle">
	
	<header class="Header">
		<h1 class="logo"><span class="smi">Charlie</span><span class="mdi"> Benjafield</span><span class="smi">&apos;s</span> Rambles</h1>
		<a class="Menu" href="#"><span>Menu</span></a>
	</header>

	<main class="Main">
		<section class="Content">
			@yield('banner')
			@yield('content')
		</section>
		<aside class="Sidebar">
			<div class="Sidebar__module">
				<h2 class="Heading__sidebar --icon +article">Recent Articles</h2>
			</div>
			<div class="Sidebar__module">
				<h2 class="Heading__sidebar --icon +series">Recent Series</h2>
			</div>
			<div class="Sidebar__module">
				<h2 class="Heading__sidebar --icon +category">Categories</h2>
			</div>
		</aside>
	</main>

</body>
</html>