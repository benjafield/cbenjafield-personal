<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="author" content="Charlie Benjafield">
	<meta name="description" content="Web Design and Development in Milton Keynes, Liverpool and Ormskirk. Get in touch for the opportunity to work together.">
	<meta name="keywords" content="web design, development, seo, milton keynes, ormskirk, liverpool">
	<title>Charlie Benjafield</title>
	<link href="{{ url('assets/css/lnr.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ url('assets/css/styles.css') }}" rel="stylesheet" type="text/css">
</head>
<body class="single-page-home">

	<header>
		<a href="#" class="logo"><span class="sr">Charlie Benjafield</span></a>
		<a href="#" class="menu-toggle"><span class="sr">Menu</span></a>
	</header>

	<nav>
		
		<div class="main">
			<a href="#home">Home</a>
			<a href="#about">About Me</a>
			<a href="#contact">Contact Me</a>
			<a href="/ramblings/">Ramblings</a>
		</div>

		<div class="social">
			<p class="s-m">
				<a href="https://www.facebook.com/cbenjafield" target="_blank"><i class="fa fa-facebook"></i></a>
				<a href="https://twitter.com/cbenjafield" target="_blank"><i class="fa fa-twitter"></i></a>
				<a href="https://uk.linkedin.com/in/cbenjafield" target="_blank"><i class="fa fa-linkedin"></i></a>
			</p>
		</div>

	</nav>
		
	<section id="home">
		<div>
			<h1>Charlie Benjafield</h1>
			<h2 class="typewriter">I make engines that run websites.</h2>
			<p class="txt-c">
				<a href="#contact" class="btn">Get in touch</a>
			</p>
		</div>
	</section>

	<section id="about" class="row">
		<div class="col-1-2 image">
			<img src="assets/images/profile-picture.jpg" alt="Charlie Benjafield Profile Picture">
		</div>
		<div class="col-1-2 about">
			<h2>About</h2>
			<p>I am a 20-year-old web developer from Milton Keynes, who is passionate about and currently studying BSc Web Systems Development at Edge Hill University in Ormskirk.</p>
			<p>I have been building websites and systems for just over 8 years, since I took a keen interest in an ICT class at school.</p>
			<p>Since then I have competed in a number of competitions, and have specialised in building web systems for businesses.</p>
		</div>
	</section>

	<section id="skills">
		<div class="dark-inner">
			<div class="container row">
				<span class="ampus">&amp;</span>
				<div class="col-1-2 txt-r design">
					<h2><i class="lnr lnr-pencil"></i> Design</h2>
					<p>
						I have always been intrigued by good designs. I saw well written and aesthetically pleasing websites everywhere and wanted to be able to build them myself.
					</p>
					<p>
						This encouraged me to learn HTML &amp; CSS, and I have been learning ever since.
					</p>
				</div>
				<div class="col-1-2 txt-l development">
					<h2><i class="lnr lnr-cog"></i> Development</h2>
					<p>
						I enjoy designing sites, and it always feels good when you design one that you are proud of, but my real passion is for developing systems.
					</p>
					<p>
						I like to build systems from the ground up whether that be a CMS or CRM, or a complete bespoke system, using frameworks or not.
					</p>
				</div>
			</div>
		</div>
	</section>

	<section id="contact">
		<div class="container row">
			<div class="col-2-5 txt-r">
				<h2>Contact</h2>
				<p class="limit">
					If you have any questions or would like to work with me on any project, big or small, then drop me a message.
				</p>
				<p class="s-m">
					<a href="https://www.facebook.com/cbenjafield" target="_blank"><i class="fa fa-facebook"></i></a>
					<a href="https://twitter.com/cbenjafield" target="_blank"><i class="fa fa-twitter"></i></a>
					<a href="https://uk.linkedin.com/in/cbenjafield" target="_blank"><i class="fa fa-linkedin"></i></a>
				</p>
			</div>
			<div class="col-3-5">
				<form action="{{ url() }}" method="post" id="contact-form">
					{!! csrf_field() !!}
					<p class="feedback">Thanks, your message has been sent.</p>
					@if (session('msg_status'))
					<p class="feedback show">{{ session('msg') }}</p>
					@endif
					@if (count($errors) > 0)
						@foreach ($errors->all() as $error)
						<p class="feedback show">{{ $error }}</p>
						@endforeach
					@endif
					<div class="form-group">
						<label class="sr" for="cf_name">Name</label>
						<input type="text" name="name" id="cf_name" placeholder="Name" value="{{ old('name') }}">
					</div>
					<div class="form-group">
						<label class="sr" for="cf_email">Email</label>
						<input type="email" name="email" id="cf_email" placeholder="Email" value="{{ old('email') }}">
					</div>
					<div class="form-group">
						<label for="cf_message" class="sr">Message</label>
						<textarea name="message" id="cf_message" placeholder="Message">{{ old('message') }}</textarea>
					</div>
					<div class="form-group txt-r">
						<label class="sr" for="humaniser">If you&apos;re human, leave this blank</label>
						<input type="checkbox" name="humaniser" id="humaniser" value="1" class="sr"> 
						<input type="submit" value="Send Message">
					</div>
				</form>
			</div>
		</div>
	</section>

	<footer class="txt-c">
		<p>
			<img src="assets/images/signature-white.png" alt="Charlie Benjafield Signature">
		</p>
		<p>
			&copy; Charlie Benjafield, {{ date('Y') }}.
		</p>
	</footer>

	<script type="text/javascript" src="{{ url('assets/js/jquery.js') }}"></script>
	<script type="text/javascript" src="{{ url('assets/js/min/cbenjafield-min.js') }}"></script>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-49758640-1', 'auto');
	  ga('send', 'pageview');

	</script>
	
</body>
</html>