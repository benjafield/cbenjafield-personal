@extends('layouts.admin')

@section('title', 'Login')

@section('content')

<div class="login-box">
	{!! Form::open(['url' => '26/03/2011/login']) !!}
	
	<h1>CB</h1>

	@if (count($errors) > 0)
		@foreach ($errors->all() as $error)
		<p class="Feedback --error">{{ $error }}</p>
		@endforeach
	@endif

	<div class="form-group">
		{!! Form::email('email', old('email'), [ 'placeholder' => 'Email Address' ]) !!}	
	</div>

	<div class="form-group">
		{!! Form::password('password', [ 'placeholder' => 'Password' ]) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Login', [ 'class' => 'Button' ]) !!}
	</div>

	{!! Form::close() !!}
</div>

@stop