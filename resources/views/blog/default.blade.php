@extends('layouts.blog')
@section('title', 'Home')
@section('body_class', 'home')

@section('banner')
<div class="Banner__image --home"></div>
@stop

@section('content')
<section class="Content__main">

	@foreach($articles as $article)
	<!-- Article -->
	<article>
		<header>
			<h1><a href="{{ url('rambles/articles/' . $article->slug) }}">{{ $article->title }}</a></h1>
			<h3>by {{ $article->user->name }} | 12 minutes ago</h3>
		</header>
		<section>
			<p>{{ substr($article->content, 0, 100) }}</p>
		</section>
		<footer>
			
		</footer>
	</article>
	<!-- /Article -->
	@endforeach

</section>
@stop

