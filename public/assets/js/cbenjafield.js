$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};
$(function () {

	$('.menu-toggle').click(function (e) {
		e.preventDefault();
		$('body').toggleClass('with-menu');
	});

	$('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('#' + this.hash.slice(1));
	      if (target.length) {
	      	$('body').removeClass('with-menu');
	        $('html,body').delay(300).animate({
	          scrollTop: target.offset().top - 120
	        }, 1000);
	        return false;
	      }
	    }
	});

	$(document).on('scroll', function () {

		var $changeHeight = 10;

		if ($(this).scrollTop() > $changeHeight) {
			$('header').addClass('scrolled');
		} else {
			$('header').removeClass('scrolled');
		}

	});

	$(document).on('submit', '#contact-form', function (e) {
	  	e.preventDefault();

	  	var $errors = 0;
	  	$(this).find('input:not([type="submit"]), textarea').each(function () {
	  		if ($(this).val() == '') {
	  			$errors++;
	  			$(this).css('border-color', '#e36767');
	  		}
	  	});

	  	if ($errors > 0) {
	  		showFeedback(0, 'Please fill out all fields.');
	  		return false;
	  	}

	  	var $formdata = $(this).serializeObject();

	  	$.ajax({
	  		url : '/',
	  		type : 'POST',
	  		dataType : 'json',
	  		data : $formdata,
	  		success : function (r, s) {
	  			if (r.code !== undefined && r.code == 1) {
	  				// Success
	  				return showFeedback(1, 'Thanks, your message has been sent.');
	  			} else {
	  				// Failed
	  				return showFeedback(0, 'Sorry, couldn&apos;t send your message right now.');
	  			}
	  		},
	  		error : function () {
	  			return showFeedback(0, 'Sorry, couldn&apos;t send your message right now.');
	  		}
	  	});

	});

});

function showFeedback (status, message) {

	$('p.feedback').fadeOut(300);

	if (status == 1) {
		$('p.feedback').html(message).fadeIn(200);
		setTimeout(function () { $('p.feedback').fadeOut(200) }, 10000);
	} else {
		$('p.feedback').html(message).fadeIn(300);
		setTimeout(function () { $('p.feedback').fadeOut(200) }, 10000);
	}

	return true;

}